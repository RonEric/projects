#pragma once

#include "ofMain.h"

class Point {
public:
	bool anker;
	//grid position
    ofVec3f position;
	ofVec3f cposition;
    ofVec3f cvelocity;
	ofVec3f cforce;
	ofVec3f accelerate;

};

class  Edge {
public:
    float k;
	float l0;
    Point *p1;
    Point *p2;
};


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofMesh mesh;
		ofEasyCam cam;

		float dt;

		ofMesh* pMesh;
		vector<Point> original_points;
		vector<Edge> edges;
		vector<Point> current_points;
		
		vector<Point> anchor_points;
		int reso;

		int width;
		int height;

		vector<int> indices;		
};
