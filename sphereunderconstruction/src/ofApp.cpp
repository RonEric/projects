#include "ofApp.h"

    
//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);
    dt = 0.010;
    int reso = 30;
    int radius = 100;
    for(int theta = 0; theta <=180; theta+=reso){
        for(int phi = 0; phi <=360; phi+=reso){
            ofVec3f vec(radius*sin(ofDegToRad(theta))*cos(ofDegToRad(phi)), radius*sin(ofDegToRad(theta))*sin(ofDegToRad(phi)),radius*cos(ofDegToRad(theta)));
            Point p;
            p.position = vec;
            p.cposition = p.position;
            original_points.push_back(p);
        }
    }
    radius = 100;
    for(int theta = 0; theta <=180; theta+=reso){
        for(int phi = 0; phi <=360; phi+=reso){
            ofVec3f vec(radius*sin(ofDegToRad(theta))*cos(ofDegToRad(phi)), radius*sin(ofDegToRad(theta))*sin(ofDegToRad(phi)),radius*cos(ofDegToRad(theta)));
            Point p;
            p.position = vec;
            //draw anchor sphere
            //p.cposition = vec;
            p.anker = true;
            anchor_points.push_back(p);
        }
    }

    width=(360+reso)/reso;
    height=(180+reso)/reso;
    

    for(int h = 0; h < height;h++){
        for(int w = 0; w < width-1; w++){   
            float k0 = ofRandom(1,3);
            Edge e;
            e.k = k0;
            e.l0 =  original_points[h*width+w].position.distance(original_points[h*width+w+1].position);
            e.p1 = &original_points[h*width+w];
            e.p2 = &original_points[h*width+w+1];
            edges.push_back(e);
        }
    }
        
        /*Edge e2;
        e.k = k0;
        e.l0 = original_points[i].position.distance(original_points[i+width].position);
        e.p1 = &original_points[i];
        e.p2 = &original_points[i+width];
        edges.push_back(e);
        
        Edge e3;
        e.k = k0;
        e.l0 = original_points[i].position.distance(original_points[i+width+1].position);
        e.p1 = &original_points[i];
        e.p2 = &original_points[i+width+1];
        edges.push_back(e);
    */
   

   for(int i = 0; i < width*height; i++){
        float k1 = ofRandom(5,20);
        Edge anchor_edge;
        anchor_edge.k=k1;
        anchor_edge.l0 = anchor_points[i].position.distance(original_points[i].position);
        anchor_edge.p1 = &anchor_points[i];
        anchor_edge.p2 = &original_points[i];
        edges.push_back(anchor_edge);
    }


}

//--------------------------------------------------------------
void ofApp::update(){
    //reset acceleration
    for(int i = 0; i < original_points.size(); i++){
        original_points[i].accelerate = ofVec3f (0,0,0);
    }
    //increment dt
    
    //derive acceleration
    for( int i = 0; i < edges.size(); i++ )
    {
        float l = (*edges[i].p1).cposition.distance((*edges[i].p2).cposition);
        float dl = l - edges[i].l0;
        float f = dl * edges[i].k;
        (*edges[i].p1).accelerate += f * ((*edges[i].p2).cposition - (*edges[i].p1).cposition).normalize();
        (*edges[i].p2).accelerate -= f * ((*edges[i].p2).cposition - (*edges[i].p1).cposition).normalize();
    }

    //derive grid acceleration
    for( int i = 0; i < original_points.size(); i++ )
    {
       original_points[i].accelerate -= 100 * (original_points[i].cposition - original_points[i].position);
    }

    //derive velocity
    for(int i = 0; i < original_points.size(); i++){
        original_points[i].cvelocity += original_points[i].accelerate * dt;
    }

    //derive current position
    for(int i = 0; i < original_points.size(); i++){
        //if(!original_points[i].anker)
            original_points[i].cposition += original_points[i].cvelocity * dt;
    }

    pMesh = new ofMesh;
    pMesh->enableIndices();
    pMesh->setMode(OF_PRIMITIVE_LINES);

    for( int i = 0; i < edges.size() - width * height; i++ )
    {
        pMesh->addVertex((*edges[i].p1).cposition);
        pMesh->addVertex((*edges[i].p2).cposition);

    }

    for(int h = 0; h<height;h++){
        for(int w = 0; w<width-1;w++){
            pMesh->addIndex(h*width+w);
        }
    }

    /*for(int i = 0; i < width*height; i++){
        int a =i;
        int b =i+1;
        int c =i+width;
        int d =i+width+1;
        pMesh->addIndex(a);
        pMesh->addIndex(b);
        pMesh->addIndex(a);
        pMesh->addIndex(c);
        pMesh->addIndex(b);
        pMesh->addIndex(d);
        pMesh->addIndex(d);
        pMesh->addIndex(c); 
        pMesh->addIndex(b);
        pMesh->addIndex(c);
    }*/
}

//--------------------------------------------------------------
void ofApp::draw(){
    cam.begin();
    //ofTranslate(ofGetWidth()/2,ofGetHeight()/2);
    
    pMesh->draw();
    cam.end();
    delete pMesh;

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
