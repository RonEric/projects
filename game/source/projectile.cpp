#include </home/ron/Projects/game/include/projectile.hpp>
#include <cmath>

Projectile::Projectile(){
        mBulletTexture = new sf::Texture();
        mBulletTexture->loadFromFile("media/bullet.png");
    }

Projectile::~Projectile(){
    delete mBulletTexture;
}

void Projectile::rotateProjectile(float angle){
    mBulletMovement.x = (1.f) * std::sin(angle + 3.1415/180);
    mBulletMovement.x = (-1.f) * std::cos(angle + 3.1415/180);    
}


sf::Texture Projectile::getBulletTexture(){
    return *mBulletTexture;
}
