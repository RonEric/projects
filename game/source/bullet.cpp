#include </home/ron/Projects/game/include/bullet.hpp>
#include <iostream>

Bullet::Bullet():
    mBullet(),
    mVelocity(100.f, 100.f){
        mShotFlag = false;
    }

void Bullet::setBulletTexture(sf::Texture texture){
    mBullet.setTexture(texture);
}

void Bullet::setBulletPosition(sf::Vector2f pos){
    mBullet.setPosition(pos);
}

void Bullet::moveBullet(sf::Vector2f playerVelocity){
    if (mShotFlag){
        mBullet.move(mVelocity);
    }
    else{
        mShotFlag = true;
        mVelocity += playerVelocity;
    }
}

sf::Vector2f Bullet::getBulletPosition(){
    return mBullet.getPosition();
}

sf::Sprite Bullet::getBulletSprite(){
    return mBullet;
}


bool Bullet::calcDistanceToPlayer(sf::Vector2f playerPos, sf::Vector2f projPos, float maxDiff){
    sf::Vector2f d = playerPos - projPos;
    if(sqrt(d.x * d.x + d.y * d.y) > maxDiff){
        return true;
    }
    return false;
}

