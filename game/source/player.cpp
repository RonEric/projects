#include </home/ron/Projects/game/include/player.hpp>   
#include <cmath>   
#include <SFML/Graphics.hpp>
   

Player::Player():
    mTexture(),
    mSprite(),
    mMovement(10.f,10.f),
    mProjectile(){
        std::vector<Bullet*> mBullet_Arr;
        mTexture.loadFromFile("../media/Eagle.png");
        mAcceleration = 10;
        mSprite.setTexture(mTexture);
    }
   


void Player::doStuff(sf::Time deltaTime){
    if (mIsMovingLeft){
        dirAngle -= 1.f;
        mSprite.rotate(-1.f);
        mIsMovingUp = false;

        this->accelerate();

        for(int i = 0; i < mBullet_Arr.size(); i++){
            mBullet_Arr[i]->getBulletSprite().rotate(-1.f);
        }
    }

    if (mIsMovingRight){
        dirAngle += 1.f;
        mSprite.rotate(1.f);
        mIsMovingUp = false;

        this->accelerate();


        for(int i = 0; i < mBullet_Arr.size(); i++){
            mBullet_Arr[i]->getBulletSprite().rotate(1.f);
        }


    }
    
    if (mIsMovingUp){
        this->accelerate();

    }

    if (mIsMovingDown){

    	float velocity = sqrt(mMovement.y * mMovement.y + mMovement.x * mMovement.x);
    	if( velocity > 10.0 )
		{
    		mMovement.x *= 0.90;
    		mMovement.y *= 0.90;

		}
    }
    
    if (mIsShooting){
        Bullet* bullet = new Bullet();
        bullet->setBulletTexture(mProjectile.getBulletTexture());
        bullet->setBulletPosition(mSprite.getPosition());
        mBullet_Arr.push_back(bullet);
    }
    
    mSprite.move(mMovement * (float) deltaTime.asMilliseconds() / 100.f);   
}

sf::Sprite Player::getPlayerSprite(){
    return mSprite;
}

std::vector<Bullet*> Player::getBulletArr(){
    return mBullet_Arr;
}

float Player::getPlayerVelocity(){
    return (mMovement.x * mMovement.x + mMovement.y + mMovement.y);
}

sf::Vector2f Player::getPlayerMovementVector(){
    return mMovement;
}

void Player::deleteBullets(){
    for(int i = 0; i < mBullet_Arr.size(); i++){
        if(mBullet_Arr[i]->calcDistanceToPlayer(mSprite.getPosition(), mBullet_Arr[i]->getBulletSprite().getPosition(),2000.f)){
            mBullet_Arr.erase(mBullet_Arr.begin()+i);
        }
    }

}

void Player::handlePlayerInput(sf::Keyboard::Key key, bool isPressed){
    if (key == sf::Keyboard::W)
        mIsMovingUp = isPressed;
    else if (key == sf::Keyboard::S)
        mIsMovingDown = isPressed;
    else if (key == sf::Keyboard::A)
        mIsMovingLeft = isPressed;
    else if (key == sf::Keyboard::D)
        mIsMovingRight = isPressed;
    else if (key == sf::Keyboard::Space)
        mIsShooting = isPressed;
}

void Player::accelerate(){
        mMovement.x = mMovement.x + mAcceleration * std::sin(dirAngle * 3.1415/180);
        mMovement.y = mMovement.y - mAcceleration * std::cos(dirAngle * 3.1415/180);
        float vel = std::sqrt(mMovement.y * mMovement.y + mMovement.x * mMovement.x);
        if( vel > 200.0 ){
        	mMovement.x = (mMovement.x / vel) * 200.0;
        	mMovement.y = (mMovement.y / vel) * 200.0;
        }
}
