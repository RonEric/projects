#include </home/ron/Projects/game/include/game.hpp>

#include <cmath>
#include <iostream>
#include <stdexcept>


const sf::Time Game::TimePerFrame = sf::seconds(1.f/60.f);

const float acceleration = 3.0;

Game::Game():
    mWorld(),
    mPlayer(),
    mView(sf::FloatRect(0,0,2000,2000)),
    mWindow(sf::VideoMode(1024, 900), "SFML Application"){
        mView.zoom(2);
        /*mPlayer;
        mTexture;
        mBulletTexture;
        mTexture.loadFromFile("media/Eagle.png");
        mPlayer.setTexture(mTexture);
        mPlayer.setPosition(0.f,0.f);
        dirAngle = 0;
        
        mIsMovingDown = false;
        mIsMovingLeft = false;
        mIsMovingUp = false;
        mIsMovingRight = false;
        mIsShooting = false;

        mMovement = sf::Vector2f(0.f,0.f);
    

        mWindow.setSize(sf::Vector2u(1024,900));
        mBackgroundTexture.loadFromFile("media/background.png");
        mBackground.setTexture(mBackgroundTexture);
        mBackgroundTexture.setRepeated(true);
        sf::Rect<int> rect(10000.f,10000.f,10000.f,10000.f);
        mBackground.setTextureRect(rect);
        mBackground.setPosition(sf::Vector2f(-5000.f, -5000.f));
        mBulletTexture.loadFromFile("media/bullet.png");
    */
}

void Game::run(){
    sf::Clock clock;
    sf::Texture background;
    
    sf::Time timeSinceLastUpdate = sf::Time::Zero;

    while (mWindow.isOpen()){
        
        //sf::Vector2f bla = mPlayer.getPosition();
        processEvents();
        timeSinceLastUpdate += clock.restart(); 
        while(timeSinceLastUpdate > TimePerFrame){
            timeSinceLastUpdate -= TimePerFrame;
            render();
            update(TimePerFrame);
        }
    }
}

void Game::processEvents(){
    sf::Event event;
    
    while (mWindow.pollEvent(event)){

        switch (event.type){
            case sf::Event::KeyPressed:
                mPlayer.handlePlayerInput(event.key.code, true);
                break;

            case sf::Event::KeyReleased:
                mPlayer.handlePlayerInput(event.key.code, false);
                break;

            case sf::Event::Closed:
                mWindow.close();
                break;    
            }

    }
}

void Game::render(){
    float x = mPlayer.getPlayerSprite().getPosition().x;
    float y = mPlayer.getPlayerSprite().getPosition().y;
    mView.setCenter(x,y);
    mWindow.clear();
    mWindow.setView(mView);
    mWindow.draw(mWorld.getWorldSprite());
    mWindow.draw(mPlayer.getPlayerSprite());
    mPlayer.deleteBullets();
    std::vector<Bullet*> bulletArr = mPlayer.getBulletArr();
    for(int i = 0; i < bulletArr.size(); i++){
        bulletArr[i]->moveBullet(mPlayer.getPlayerMovementVector());
        mWindow.draw(bulletArr[i]->getBulletSprite());
    }
    mWindow.display();
}

void Game::update(sf::Time deltaTime){
    mPlayer.doStuff(deltaTime);
}

