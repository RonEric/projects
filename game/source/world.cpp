#include </home/ron/Projects/game/include/world.hpp>

World::World():
    mSize(10000.f,10000.f,10000.f,10000.f){
        mBackgroundTexture.loadFromFile("../media/background.png");
        mBackground.setTexture(mBackgroundTexture);
        mBackgroundTexture.setRepeated(true);
        mBackground.setTextureRect(mSize);
        mBackground.setPosition(sf::Vector2f(-5000.f, -5000.f));
    }

sf::Sprite    World::getWorldSprite(){
    return mBackground;
}

World::~World(){

}