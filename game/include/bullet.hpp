#include </home/ron/Projects/game/source/projectile.cpp>


class Bullet : Projectile{
    public:
        Bullet();
       //~Bullet();

    private:
        sf::Sprite      mBullet;
        sf::Vector2f    mPosition;
        sf::Vector2f    mVelocity;
        bool            mShotFlag;
        
    
    public:
        void            setBulletTexture(sf::Texture texture);
        void            setBulletPosition(sf::Vector2f pos);
        
        sf::Vector2f    getBulletPosition();
        
        sf::Sprite      getBulletSprite();

        void            moveBullet(sf::Vector2f playerVeclocity);
        bool            calcDistanceToPlayer(sf::Vector2f playerPos, sf::Vector2f projPos, float maxDiff);

};