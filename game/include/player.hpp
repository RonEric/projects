#include <SFML/Graphics.hpp>
#include </home/ron/Projects/game/source/bullet.cpp>
#include <vector>
class Player{
    public:
        Player();
        //virtual ~Player();

    public:
        void                    handlePlayerInput(sf::Keyboard::Key key, bool isPressed);
        void                    doStuff(sf::Time deltaTime);
        sf::Sprite              getPlayerSprite();
        std::vector<Bullet*>    getBulletArr();
        float                   getPlayerVelocity();
        sf::Vector2f            getPlayerMovementVector();
        void                    deleteBullets();




    private:
        bool    mIsMovingUp;
        bool    mIsMovingDown;
        bool    mIsMovingLeft;
        bool    mIsMovingRight;
        bool    mIsShooting;

        float   dirAngle;

        sf::Sprite                  mSprite;
        sf::Texture                 mTexture;
        sf::Vector2f                mMovement;
        std::vector<Bullet*>        mBullet_Arr;

        Projectile  mProjectile;

        float       mAcceleration;
        
        void        accelerate();


};