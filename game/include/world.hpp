#include <SFML/Graphics.hpp>

class World{
    public: 
        World();
        virtual ~World();
    
    private:
        sf::IntRect         mSize;
        sf::Sprite          mBackground;
        sf::Texture         mBackgroundTexture;

    public:
        sf::Sprite          getWorldSprite();

};