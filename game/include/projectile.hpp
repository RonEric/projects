#include <SFML/Graphics.hpp>


class Projectile{    
    public: 
                    Projectile();
        virtual    ~Projectile();
    
    public:
        void            rotateProjectile(float angle);
        sf::Texture     getBulletTexture();

    private:
        sf::Vector2f                mBulletMovement;
        sf::Texture*                mBulletTexture;
};
    