#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <vector>
#include </home/ron/Projects/game/source/world.cpp>
#include </home/ron/Projects/game/source/player.cpp>


class Game{
    public:
                Game();
        void    run();

    private:
        void    processEvents();
        void    update(sf::Time deltaTime);
        void    render();


    private:
        static const sf::Time            TimePerFrame;
        
        sf::View            mView;
        sf::RenderWindow    mWindow;
        World               mWorld;
        Player              mPlayer;

        
};  