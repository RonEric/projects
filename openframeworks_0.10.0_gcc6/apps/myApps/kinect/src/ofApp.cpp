#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    kinect.init();
    kinect.open();
    kinect.setCameraTiltAngle(12);

}

//--------------------------------------------------------------
void ofApp::update(){
    kinect.update();
    mesh = new ofMesh();
    mesh->setMode(OF_PRIMITIVE_POINTS);
    for(int h = 0; h < kinect.getHeight(); h+=10){
            for(int w = 0; w < kinect.getWidth(); w+=10){
                mesh->addVertex(ofVec3f(kinect.getWorldCoordinateAt(w,h).x,kinect.getWorldCoordinateAt(w,h).y,kinect.getWorldCoordinateAt(w,h).z));
                cout << kinect.getWorldCoordinateAt(w,h) << endl;
                mesh->addColor(ofColor(0,0,0));
            }
     }

}

//--------------------------------------------------------------
void ofApp::draw(){
    ofTranslate(kinect.getWidth()/2,kinect.getHeight()/2);
    mesh->draw();
    delete mesh;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
