#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <iostream>
#include <arpa/inet.h>
#include <unistd.h>
#include <cerrno>

/*void send_(int socket_fd, char *argv[]){
	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(atoi(argv[3]));
	serv_addr.sin_addr.s_addr = inet_addr(argv[2]);

	if(connect(socket_fd,(struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
		std::cout << "error connecting\n";
	} else {
		std::cout << "working\n";
	}
}*/

void recv_(int socket_fd, char argv[]){
	struct sockaddr_in sock_addr;
        sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(atoi(argv));
	sock_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	if(bind(socket_fd,(struct sockaddr*)&sock_addr, sizeof(sock_addr)) < 0 ){
		std::cout << errno;
		close(socket_fd);
		if (errno == EINVAL){
			std::cout << errno;
		}
	}else{
		listen(socket_fd, 1);
		socklen_t size = sizeof(sock_addr);
		int l_socket = accept(socket_fd, (struct sockaddr*)&sock_addr, &size);
		char buffer[64];
		read(l_socket, buffer, 64);
		std::cout << buffer;
		close(l_socket);
	}
}

int main(int argc, char *argv[]){
	int socket_fd;
	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	recv_(socket_fd, argv[1]);
	close(socket_fd);
}

