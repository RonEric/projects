#include<iostream>

void printHelloWorld(){
	std::cout << "Hello World!\n";
}

void printByeBye(){
	std::cout << "Bye By3!\n";
}

void exit(){
	std::cout << "Exiting\n";
}

void printSelection(){
	std::cout << "1. say Hello!\n";
	std::cout << "2. Bye Bye!\n";
	std::cout << "3. Exit\n";
	std::cout << "Select: ";
}
int main(){
	printSelection();

	int input;
	bool running = true;

	while(running){
		std::cin >> input;
		switch(input){
			case 1:
				printHelloWorld();
				printSelection();
				break;
			case 2:
				printByeBye();
				printSelection();
				break;
			case 3:
				exit();	
				printSelection();
				running = false;
				break;
		}
	}
 	std::cin.get();
	return 0;
	
}


