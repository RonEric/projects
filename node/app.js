const http = require('http');
const fs = require('fs');
const hostname = '127.0.0.1';
const port = 3000;


const server = http.createServer((req, res) => {
  var path = "/home/ron/Projects/node" + req.url;
  fs.readFile(path, (err, data) => {
    if (err) {
      res.setHeader("Content-type","text/plain");
      res.end(path);
    }else{
      res.setHeader("Content-type","text/html");
      res.end(data);
    }
  });
});
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
